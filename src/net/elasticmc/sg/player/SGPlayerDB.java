package net.elasticmc.sg.player;

import ch.playat.rushmead.elasticmc.database.DatabaseConnections;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Rushmead
 */
public class SGPlayerDB {

    /**
     * Add a player (with default hub player data) to the database
     *
     * @param uuid The UUID of the player (in string form)
     */
    public synchronized void addPlayerToDatabase(String uuid) {
        if (isInDatabase(uuid)) {
            return;
        }
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO `playerdata_sg`(`uuid`) VALUES (?)");
            ps.setString(1, uuid);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Check if a UUID is already in the database
     *
     * @param uuid The player UUID (in string form)
     * @return Is the player in the database?
     */
    public synchronized boolean isInDatabase(String uuid) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT `uuid` FROM `playerdata_sg` WHERE `uuid` = ?");
            ps.setString(1, uuid);
            
            ResultSet rs = ps.executeQuery();
            boolean has = rs.next();
            
            if (has) {
                return true;
            }
        } catch (SQLException ex) {
            
        }
        
        return false;
    }

    /**
     * Return an Hub Player from a Player's name
     *
     * @param p The player
     * @return The Hub player
     */
    public synchronized SGPlayer getPlayer(Player p) {
        SGPlayer player = null;
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `playerdata_sg` WHERE `uuid` = ?");
            ps.setString(1, p.getUniqueId().toString());
            
            ResultSet rs = ps.executeQuery();
            boolean has = rs.next();
            
            if (has) {
                SGPlayer sgPlayer = new SGPlayer(p.getDisplayName(), p.getUniqueId().toString(), rs.getInt("kills"), rs.getInt("deaths"), rs.getInt("wins"), rs.getInt("games"), rs.getInt("sponsor_tokens"));
                return sgPlayer;
            } else {
                throw new SQLException();
            }
        } catch (SQLException ex) {
            addPlayerToDatabase(p.getUniqueId().toString());
            player = new SGPlayer(p.getDisplayName(), p.getUniqueId().toString());
        }
        
        return player;
    }

    /**
     * Save a player's information to the database
     *
     * @param player The player you are saving to the database
     */
    public synchronized void savePlayerToDB(SGPlayer player) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE `playerdata_sg` SET `wins`=?,`kills`=?,`games`=?,`deaths`=?,`sponsor_tokens`=? WHERE `uuid` = ?");
            ps.setInt(1, player.getWins());
            ps.setInt(2, player.getKills());
            ps.setInt(3, player.getGames());
            ps.setInt(4, player.getDeaths());
            ps.setInt(5, player.getSponsorTokens());
            ps.setString(6, player.getUuid());
            ps.executeUpdate();
            ps.close();
        } catch (NullPointerException ex) {
            
        } catch (SQLException ex) {
            this.addPlayerToDatabase(player.getPlayer().getUniqueId().toString());
        }
    }
}
