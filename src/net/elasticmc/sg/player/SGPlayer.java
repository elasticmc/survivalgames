package net.elasticmc.sg.player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;

/**
 * @author Rushmead
 */
public class SGPlayer {

    private static SGPlayerDB hpDB = new SGPlayerDB();
    private static HashMap<String, SGPlayer> players = new HashMap<>();

    public static HashMap<String, SGPlayer> getPlayers() {
        return players;
    }

    public static SGPlayer getPlayer(String name) {
        if (!players.containsKey(name)) {
            return null;
        }
        return players.get(name);
    }

    public static SGPlayerDB getLaserDB() {
        return hpDB;
    }

    public static SGPlayer createPlayer(Player p) {
        if (!players.containsKey(p.getDisplayName())) {
            SGPlayer player = hpDB.getPlayer(p);
            if (player == null && p != null) {
                player = new SGPlayer(p.getDisplayName(), p.getUniqueId().toString());
            }
            players.put(p.getDisplayName(), player);
            return player;
        } else {
            return getPlayer(p.getDisplayName());
        }

    }

    public static void removePlayer(String name) {
        if (players.containsKey(name)) {
            players.remove(name);

        }
    }

    private String realName;
    private String uuid;
    private int kills;
    private int deaths;
    private int wins;
    private int games;
    private int currentKills;
    private int sponsorTokens;

    public SGPlayer(String name, String uuid) {
        this.uuid = uuid;
        this.realName = name;
        this.kills = 0;
        this.deaths = 0;
        this.wins = 0;
        this.games = 0;
        this.currentKills = 0;
        this.sponsorTokens = 1;
    }

    public SGPlayer(String name, String uuid, int kills, int deaths, int wins, int games, int sponsorTokens) {
        this.realName = name;
        this.uuid = uuid;
        this.kills = kills;
        this.deaths = deaths;
        this.wins = wins;
        this.games = games;
        this.currentKills = 0;
        this.sponsorTokens = sponsorTokens;
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(getRealName());
    }

    /**
     * @return the realName
     */
    public String getRealName() {
        return realName;
    }

    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }

    public String getUuid() {
        return uuid;
    }

    public int getCurrentKills() {
        return currentKills;
    }

    public void setCurrentKills(int currentKills) {
        this.currentKills = currentKills;
    }

    /**
     * @return the sponsorTokens
     */
    public int getSponsorTokens() {
        return sponsorTokens;
    }

    /**
     *
     */
    public void addSponsorToken() {
        this.sponsorTokens += 1;
    }

    public void removeSponsorToken() {
        this.sponsorTokens -= 1;
    }
}
