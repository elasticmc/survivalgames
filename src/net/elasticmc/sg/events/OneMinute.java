package net.elasticmc.sg.events;

import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.customevents.OneMinuteTimerEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 *
 * @author Rushmead
 */
public class OneMinute implements Listener {

    @EventHandler
    public void onMinute(OneMinuteTimerEvent e) {
        if (GameState.getGameState() == GameState.IN_GAME) {

        }
    }
}
