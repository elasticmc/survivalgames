package net.elasticmc.sg.events;

import ch.playat.rushmead.elasticmc.framework.api.ElasticCommand;
import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.customevents.FrameworkEnableEvent;
import net.elasticmc.sg.commands.GameCommand;
import net.elasticmc.sg.commands.SponsorCommand;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 * @author Rushmead
 */
public class FrameworkEnable implements Listener {
    
    @EventHandler
    public void onFrameworkEnable(FrameworkEnableEvent e) {
        ElasticCommand.registerCommand("sg", "game", new GameCommand());
        ElasticCommand.registerCommand("sg", "sponsor", new SponsorCommand());
        GameState.setGameState(GameState.LOBBY);
    }
}
