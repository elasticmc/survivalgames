package net.elasticmc.sg.events;

import ch.playat.rushmead.elasticmc.framework.api.GameState;
import net.elasticmc.sg.ElasticSG;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 *
 * @author Rushmead
 */
public class InventoryClick implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (GameState.getGameState() == GameState.IN_GAME) {
            if (ElasticSG.getInstance().getGameManager().spectators.contains(e.getWhoClicked().getName())) {
                e.setCancelled(true);
                return;
            }
            e.setCancelled(false);
        } else {
            e.setCancelled(false);
        }
    }
}
