package net.elasticmc.sg.events.players;

import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.items.FrameworkItems;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import ch.playat.rushmead.elasticmc.framework.util.PlayerUtil;
import net.elasticmc.sg.ElasticSG;
import net.elasticmc.sg.player.SGPlayer;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

/**
 * Created by Rushmead for ElasticMC
 */
public class PlayerDeath implements Listener {

    @EventHandler
    public void playerDeath(PlayerDeathEvent e) {
        e.setKeepInventory(false);
        if (GameState.getGameState() == GameState.IN_GAME) {
            if (e.getEntity() instanceof Player) {
                final Player killed = (Player) e.getEntity();
                SGPlayer killedSGPlayer = SGPlayer.getPlayer(killed.getDisplayName());
                ElasticPlayer killedEPlayer = ElasticPlayer.getPlayer(killed.getDisplayName());
                if (ElasticSG.getInstance().getGameManager().players.contains(e.getEntity().getDisplayName())) {
                    e.setDeathMessage("");
                    if (e.getEntity().getKiller() instanceof Player) {
                        final Location position = killed.getLocation();
                        Player killer = killed.getKiller();
                        SGPlayer killerSGPlayer = SGPlayer.getPlayer(killer.getDisplayName());
                        ElasticPlayer killerEPlayer = ElasticPlayer.getPlayer(killer.getDisplayName());
                        killed.getWorld().strikeLightning(killed.getLocation());
                        Messaging.broadcastMessage("&6&l{0}&r&a was killed by &6&l{1}!", killed.getName(), killed.getKiller().getDisplayName());
                        killedSGPlayer.setDeaths(killedSGPlayer.getDeaths() + 1);
                        killerSGPlayer.setCurrentKills(killerSGPlayer.getCurrentKills() + 1);
                        killerSGPlayer.setKills(killerSGPlayer.getKills() + 1);
                        if(killerSGPlayer.getKills() %2 == 0)
                        {
                            killerSGPlayer.addSponsorToken();
                        }
                        killedEPlayer.setInSpecChat(true);
                        killedEPlayer.setChatPrefix("&4Spec &8\u23aa");
                        killed.setGameMode(GameMode.SPECTATOR);
                        killed.getInventory().setItem(8, FrameworkItems.backToHub());
                        PlayerUtil.forceRespawnPlayer(killed);
                        Bukkit.getScheduler().scheduleSyncDelayedTask(
                                ElasticSG.getInstance(),
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        PlayerUtil.forceRespawnPlayer(killed);
                                        killed.teleport(position);
                                    }
                                }, 2 * 20L);

                    } else {
                        final Location position = killed.getLocation();
                        killed.getWorld().strikeLightning(killed.getLocation());
                        Messaging.broadcastMessage("&6&l{0}&r&a has died of natural causes!", killed.getName());
                        killedSGPlayer.setDeaths(killedSGPlayer.getDeaths() + 1);
                        killedEPlayer.setInSpecChat(true);
                        killedEPlayer.setChatPrefix("&4Spec &8\u23aa");
                        killed.setGameMode(GameMode.SPECTATOR);
                        killed.getInventory().setItem(8, FrameworkItems.backToHub());
                        Bukkit.getScheduler().scheduleSyncDelayedTask(
                                ElasticSG.getInstance(),
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        PlayerUtil.forceRespawnPlayer(killed);
                                        killed.teleport(position);
                                    }
                                }, 2 * 20L);

                    }
                    ElasticSG.getInstance().getGameManager().players.remove(killed.getName());
                    ElasticSG.getInstance().getGameManager().spectators.add(killed.getName());
                    if (ElasticSG.getInstance().getGameManager().players.size() == 1) {
                        ElasticSG.getInstance().getGameManager().stop();
                    }
                }
            }
        }
    }
}
