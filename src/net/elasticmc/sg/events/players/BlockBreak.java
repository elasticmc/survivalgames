package net.elasticmc.sg.events.players;

import ch.playat.rushmead.elasticmc.framework.api.GameState;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

/**
 *
 * @author Rushmead
 */
public class BlockBreak implements Listener {
    
    @EventHandler
    public void onBlockBreakEvent(BlockBreakEvent e) {
        
        if (GameState.getGameState() == GameState.IN_GAME) {
            if (e.getBlock().getType() == Material.LEAVES || e.getBlock().getType() == Material.DIRT) {
                e.setCancelled(false);
            } else {
                e.setCancelled(true);
            }
        }
    }
}
