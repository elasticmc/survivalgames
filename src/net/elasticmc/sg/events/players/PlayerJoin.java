package net.elasticmc.sg.events.players;

import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import net.elasticmc.sg.ElasticSG;
import net.elasticmc.sg.lobby.LobbyItems;
import net.elasticmc.sg.player.SGPlayer;
import net.elasticmc.sg.util.Scoreboard;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;

/**
 *
 * @author Rushmead
 */
public class PlayerJoin implements Listener {

    public void togglePlayerVisibility(Player p, boolean visible) {
        for (Player o : Bukkit.getOnlinePlayers()) {
            if (visible) {
                p.showPlayer(o);
            } else {
                p.hidePlayer(o);
            }
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        SGPlayer lp = SGPlayer.createPlayer(e.getPlayer());
        Messaging.broadcastMessage("{0} has Joined, {1}/{2}.", e.getPlayer().getName(), Bukkit.getOnlinePlayers().size(), 24);
        if (Bukkit.getOnlinePlayers().size() >= 12) {
            Messaging.broadcastMessage("Minimum players reached. Starting Countdown...");
            ElasticSG.getInstance().getLobbyManager().startCountdown();
        }

<<<<<<< HEAD
        e.getPlayer().teleport(new Location(Bukkit.getWorlds().get(0), -186, 68, -174));
=======
        e.getPlayer().teleport(new Location(Bukkit.getWorlds().get(0), -185, 68 , -172));
>>>>>>> 639dd1eeb43b66102f2b22b644a9035d70870cf8
        e.getPlayer().getInventory().clear();
        e.getPlayer().getInventory().setArmorContents(null);
        e.getPlayer().setGameMode(GameMode.ADVENTURE);
        for (PotionEffect pe : e.getPlayer().getActivePotionEffects()) {
            e.getPlayer().removePotionEffect(pe.getType());
        }
        for (int i = 0; i < LobbyItems.getInventory().length; i++) {
            if (LobbyItems.getInventory()[i] == null) {
                continue;
            }
            e.getPlayer().getInventory().setItem(i, LobbyItems.getInventory()[i]);
        }
        if (GameState.getGameState() == GameState.LOBBY) {
            for (Player ps : Bukkit.getOnlinePlayers()) {
                Scoreboard.lobby(ps);
            }
        }
        ElasticSG.getInstance().getGameManager().players.add(lp.getRealName());
        togglePlayerVisibility(e.getPlayer(), true);
    }
}
