package net.elasticmc.sg.events.players;

import ch.playat.rushmead.elasticmc.database.players.PermissionSet;
import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import net.elasticmc.sg.ElasticSG;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;

/**
 *
 * @author Rushmead
 */
public class BlockPlace implements Listener {

    @EventHandler
    public void onBlockEvent(BlockPlaceEvent e) {
        if (e.getBlock().getType() == Material.REDSTONE_TORCH_ON) {
            Messaging.sendMessage(e.getPlayer(), "&aCalling in CarePackage... ETA 30 Seconds...");
            Location l = e.getBlock().getLocation();
            l.add(0, 30, 0);
            ElasticSG.getInstance().getGameManager().carePackages.put(l, 30);
            e.setCancelled(false);
        } else {
            if (GameState.getGameState() == GameState.IN_GAME) {
                e.setCancelled(true);
            } else if (ElasticPlayer.getPlayer(e.getPlayer().getName()).getPermissions().getPower() >= PermissionSet.JNR_STAFF.getPower()) {
                e.setCancelled(false);
            }

        }
    }

    @EventHandler
    public void onBlockBurn(BlockBurnEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onBlockSpread(BlockSpreadEvent event) {
        if (event.getBlock().getTypeId() != 3) {
            event.setCancelled(true);
        }
    }
}
