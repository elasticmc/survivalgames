package net.elasticmc.sg.events.players;

import ch.playat.rushmead.elasticmc.framework.api.GameState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Created by Rushmead for ElasticMC
 */
public class PlayerDamage implements Listener {
    @EventHandler
    public void onDamage(EntityDamageEvent e){
        if(GameState.getGameState() == GameState.IN_GAME){
            e.setCancelled(false);
        }else{
            e.setCancelled(true);
        }
    }

}
