package net.elasticmc.sg.events.players;

import net.elasticmc.sg.ElasticSG;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author Rushmead
 */
public class PlayerInteract implements Listener {
    
    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player player = (Player) e.getPlayer();
        
        if (e.getAction() == Action.RIGHT_CLICK_AIR) {
            if (e.getItem() != null && e.getItem().getType() == Material.EYE_OF_ENDER) {
                e.setCancelled(true);
            }
            
        }
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getClickedBlock().getType() == Material.CHEST) {
                Chest c = (Chest) e.getClickedBlock().getState();
                
                boolean empty = true;
                for (ItemStack item : c.getInventory().getContents()) {
                    if (item != null) {
                        empty = false;
                        break;
                    }
                }
                if (empty) {
                    if (!ElasticSG.getInstance().getLootManager().getChests().contains(c.getBlock().getLocation())) {
                        ElasticSG.getInstance().getLootManager().fillChests(c.getBlockInventory());
                    }
                }
                
            }
        }
    }
}
