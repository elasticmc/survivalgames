package net.elasticmc.sg.events;

import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.customevents.OneSecondTimerEvent;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import ch.playat.rushmead.elasticmc.framework.util.PlayerUtil;
import net.elasticmc.sg.ElasticSG;
import net.elasticmc.sg.util.Scoreboard;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.concurrent.TimeUnit;
import net.elasticmc.sg.player.SGPlayer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;

/**
 * @author Rushmead
 */
public class OneSecond implements Listener {

    public static int time = 0;

    @EventHandler
    public void onOneSecond(OneSecondTimerEvent e) {

        time++;
        if (Bukkit.getOnlinePlayers().size() > 0) {

            if (GameState.getGameState() == GameState.SETUP) {

                if (ElasticSG.getInstance().getGameManager().countdown) {
                    if (ElasticSG.getInstance().getGameManager().countDownTime == 0) {
                        ElasticSG.getInstance().getGameManager().start();
                        return;
                    }
                    if (ElasticSG.getInstance().getGameManager().countDownTime <= 10) {
                        for (Player p : Bukkit.getOnlinePlayers()) {
                            p.playSound(p.getLocation(), Sound.CLICK, 1, 1);
                            PlayerUtil.sendTitleOrSubtitle(p, PacketPlayOutTitle.EnumTitleAction.TITLE, "&l" + ElasticSG.getInstance().getGameManager().countDownTime + "", 0, 20, 0, ChatColor.GOLD);
                        }
                    }
                    ElasticSG.getInstance().getGameManager().countDownTime -= 1;
                }

            } else if (GameState.getGameState() == GameState.IN_GAME) {
                if (ElasticSG.getInstance().getGameManager().gameTime != 0 && !ElasticSG.getInstance().getGameManager().deathmatch) {
                    if (ElasticSG.getInstance().getGameManager().gameTime - ElasticSG.getInstance().getGameManager().refillTime == 0 && !ElasticSG.getInstance().getGameManager().refilled) {
                        Messaging.broadcastMessage("&aCHESTS HAVE BEEN REFILLED!");
                        ElasticSG.getInstance().getLootManager().populateChests();
                        ElasticSG.getInstance().getGameManager().refilled = true;
                    }
                    if (ElasticSG.getInstance().getGameManager().gameTime % 60 == 0) {

                        if (ElasticSG.getInstance().getGameManager().gameTime >= 60) {
                            Messaging.broadcastMessage("&6&l{0}&r &aminutes remaining till deathmatch", TimeUnit.SECONDS.toMinutes(ElasticSG.getInstance().getGameManager().gameTime));
                        } else {
                            Messaging.broadcastMessage("&6&l{0}&r &aminute remaining till deathmatch", TimeUnit.SECONDS.toMinutes(ElasticSG.getInstance().getGameManager().gameTime));
                        }
                    }
                    if (ElasticSG.getInstance().getGameManager().gameTime == 30) {
                        Messaging.broadcastMessage("&6&l{0}&r &aseconds remaining till deathmatch", ElasticSG.getInstance().getGameManager().gameTime);
                    }
                    ElasticSG.getInstance().getGameManager().gameTime--;

                }
                if (ElasticSG.getInstance().getGameManager().gameTime == 0 && !ElasticSG.getInstance().getGameManager().deathmatch) {

                    Messaging.broadcastMessage("&aLet Deathmatch.. &lBEGIN!");
                    ElasticSG.getInstance().getGameManager().startDeathmatch();

                }
                for (Player p : Bukkit.getOnlinePlayers()) {
                    Scoreboard.game(p);
                }
                if(time % 30 == 0){
                    for(String s : ElasticSG.getInstance().getGameManager().spectators){
                        Messaging.sendMessage(Bukkit.getPlayer(s), "&aYou have &6&l{0} &r&asponsor tokens, use /sponsor <username> to sponsor a user!", SGPlayer.getPlayer(s).getSponsorTokens());
                        
                    }
                }
                for (Location l : ElasticSG.getInstance().getGameManager().carePackages.keySet()) {
                    if (ElasticSG.getInstance().getGameManager().carePackages.get(l) == null) {
                        ElasticSG.getInstance().getGameManager().carePackages.remove(l);
                        continue;
                    }
                    int original = ElasticSG.getInstance().getGameManager().carePackages.get(l);
                    if (Bukkit.getWorld(ElasticSG.getInstance().getMapManager().getCurrentMap().getWorldName()).getBlockAt(l) != null && Bukkit.getWorld(ElasticSG.getInstance().getMapManager().getCurrentMap().getWorldName()).getBlockAt(l).getType() == Material.CHEST && original >= 1) {
                        Bukkit.getWorld(ElasticSG.getInstance().getMapManager().getCurrentMap().getWorldName()).getBlockAt(l).setType(Material.AIR);
                    }
                    Location oldLocation = l;
                    Location newLocation = l.subtract(0, 1, 0);
                    Bukkit.getWorld(ElasticSG.getInstance().getMapManager().getCurrentMap().getWorldName()).getBlockAt(oldLocation).setType(Material.AIR);
                    int newOne = original - 1;
                    Bukkit.getWorld(ElasticSG.getInstance().getMapManager().getCurrentMap().getWorldName()).getBlockAt(newLocation).setType(Material.CHEST);
                    if (newOne == 0) {

                        ElasticSG.getInstance().getGameManager().carePackages.remove(oldLocation, original);
                        if (Bukkit.getWorld(ElasticSG.getInstance().getMapManager().getCurrentMap().getWorldName()).getBlockAt(oldLocation).getType() == Material.REDSTONE_TORCH_ON) {
                            Bukkit.getWorld(ElasticSG.getInstance().getMapManager().getCurrentMap().getWorldName()).getBlockAt(oldLocation).setType(Material.AIR);
                        }
                        if (Bukkit.getWorld(ElasticSG.getInstance().getMapManager().getCurrentMap().getWorldName()).getBlockAt(oldLocation).getType() == Material.CHEST) {
                            Chest c = (Chest) Bukkit.getWorld(ElasticSG.getInstance().getMapManager().getCurrentMap().getWorldName()).getBlockAt(oldLocation).getState();
                            ElasticSG.getInstance().getLootManager().fillChests(c.getBlockInventory());
                            ElasticSG.getInstance().getLootManager().getChests().add(c.getLocation());
                        }

                    } else {
                        ElasticSG.getInstance().getGameManager().carePackages.remove(oldLocation, original);
                        ElasticSG.getInstance().getGameManager().carePackages.put(newLocation, newOne);
                    }

                }
                if (ElasticSG.getInstance().getGameManager().countdown) {
                    if (ElasticSG.getInstance().getGameManager().countDownTime == 0) {
                        ElasticSG.getInstance().getGameManager().dmStrt();
                        return;
                    }
                    if (ElasticSG.getInstance().getGameManager().countDownTime <= 10) {
                        for (Player p : Bukkit.getOnlinePlayers()) {
                            p.playSound(p.getLocation(), Sound.CLICK, 1, 1);
                            PlayerUtil.sendTitleOrSubtitle(p, PacketPlayOutTitle.EnumTitleAction.TITLE, "&l" + ElasticSG.getInstance().getGameManager().countDownTime + "", 0, 20, 0, ChatColor.GOLD);
                        }
                    }
                    ElasticSG.getInstance().getGameManager().countDownTime -= 1;
                }
            } else if (GameState.getGameState() == GameState.LOBBY) {
                if (time % 30 == 0) {
                    Messaging.broadcastMessage("&aPlaying Map: &6{0} &aby &6{1}", ElasticSG.getInstance().getMapManager().getCurrentMap().getMapName(), ElasticSG.getInstance().getMapManager().getCurrentMap().getAuthors());
                }
                if (ElasticSG.getInstance().getLobbyManager().countdown) {
                    if (ElasticSG.getInstance().getLobbyManager().countDownTime == 0) {
                        ElasticSG.getInstance().getGameManager().prep();
                        return;
                    }
                    if (ElasticSG.getInstance().getLobbyManager().countDownTime == 60) {
                        Messaging.broadcastMessage("&6&l{0}&r &aseconds till game start", ElasticSG.getInstance().getLobbyManager().countDownTime);
                    }
                    if (ElasticSG.getInstance().getLobbyManager().countDownTime == 30) {
                        Messaging.broadcastMessage("&6&l{0}&r &aseconds till game start", ElasticSG.getInstance().getLobbyManager().countDownTime);
                    }
                    if (ElasticSG.getInstance().getLobbyManager().countDownTime <= 10) {
                        Messaging.broadcastMessage("&6&l{0}&r &aseconds till game start", ElasticSG.getInstance().getLobbyManager().countDownTime);
                    }

                    ElasticSG.getInstance().getLobbyManager().countDownTime -= 1;

                }
            }

        }
    }
}
