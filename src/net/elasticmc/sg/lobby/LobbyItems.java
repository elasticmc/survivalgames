package net.elasticmc.sg.lobby;

import ch.playat.rushmead.elasticmc.framework.items.FrameworkItems;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Rushmead
 */
public class LobbyItems {

    public static ItemStack[] getInventory() {
        ItemStack[] items = new ItemStack[9];
        items[8] = FrameworkItems.backToHub();
        return items;
    }
}
