package net.elasticmc.sg.util;

import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import net.elasticmc.sg.ElasticSG;
import net.elasticmc.sg.player.SGPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Objective;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Rushmead
 */
public class Scoreboard {

    private static org.bukkit.scoreboard.Scoreboard sb = ElasticSG.getInstance().getServer().getScoreboardManager().getNewScoreboard();
    private static Objective lobby = null;

    public static org.bukkit.scoreboard.Scoreboard getScoreboard() {
        return sb;
    }

    public static void lobby(Player player) {
        if (SimpleScoreboard.getManager().getScoreboard(player) == null) {
            SimpleScoreboard.getManager().setupBoard(player);
        }
        if (SimpleScoreboard.getManager().getScoreboard(player) != null) {
            SimpleScoreboard.getManager().getScoreboard(player).unregister();
            SimpleScoreboard.getManager().setupBoard(player);
        }
        SimpleScoreboard.getManager().getScoreboard(player).setHeader(ChatColor.BLUE + "   " + Messaging.colorizeMessage("&3&LElastic&5&lMC") + "   ");
        SimpleScoreboard.getManager().getScoreboard(player).setScores(Messaging.colorizeMessage("&3Game:"), Messaging.colorizeMessage("&aSurvival Games"), Messaging.colorizeMessage("&3Kills:"), Messaging.colorizeMessage("&6&l" + SGPlayer.getPlayer(player.getName()).getKills()), Messaging.colorizeMessage("&3Map:"), Messaging.colorizeMessage("&6&l" + ElasticSG.getInstance().getMapManager().getCurrentMap().getMapName()), Messaging.colorizeMessage("&3Players:"), Messaging.colorizeMessage("&6&l" + ElasticSG.getInstance().getGameManager().players.size() + "/24"));
        SimpleScoreboard.getManager().getScoreboard(player).update();
    }

    public static void game(Player player) {
        if (SimpleScoreboard.getManager().getScoreboard(player) != null) {
            SimpleScoreboard.getManager().getScoreboard(player).unregister();
        }
        SimpleScoreboard.getManager().setupBoard(player);
        SimpleScoreboard.getManager().getScoreboard(player).setHeader(ChatColor.BLUE + "   " + Messaging.colorizeMessage("&3&LElastic&5&lMC") + "   ");
        List<String> scores = new ArrayList<>();
        scores.add(Messaging.colorizeMessage("&5Game:"));
        scores.add(Messaging.colorizeMessage("&a&lSurvival Games"));
        scores.add(Messaging.colorizeMessage("&5Kills"));
        scores.add(Messaging.colorizeMessage("&2&l" + SGPlayer.getPlayer(player.getName()).getCurrentKills()));
        scores.add(Messaging.colorizeMessage("&5Players Remaining"));
        scores.add(Messaging.colorizeMessage("&a&l" + ElasticSG.getInstance().getGameManager().players.size()));
        scores.add(Messaging.colorizeMessage("&5Spectators"));
        scores.add(Messaging.colorizeMessage("&a&l" + ElasticSG.getInstance().getGameManager().spectators.size()));
        scores.add(Messaging.colorizeMessage("&5Time till deathmatch"));
        scores.add(Messaging.colorizeMessage("&a&l" + TimeUnit.SECONDS.toMinutes(ElasticSG.getInstance().getGameManager().gameTime) + ":" + ElasticSG.getInstance().getGameManager().gameTime % 60));
        if (!ElasticSG.getInstance().getGameManager().refilled) {
            scores.add(Messaging.colorizeMessage("&5Chest Refills"));
            scores.add(Messaging.colorizeMessage("&a&l" + TimeUnit.SECONDS.toMinutes(ElasticSG.getInstance().getGameManager().gameTime - ElasticSG.getInstance().getGameManager().refillTime) + ":" + (ElasticSG.getInstance().getGameManager().gameTime - ElasticSG.getInstance().getGameManager().refillTime) % 60));
        }

        SimpleScoreboard.getManager().getScoreboard(player).setScores(scores);
        SimpleScoreboard.getManager().getScoreboard(player).update();
    }
}
