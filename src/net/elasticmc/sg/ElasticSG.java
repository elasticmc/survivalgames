package net.elasticmc.sg;

import ch.playat.rushmead.elasticmc.database.Servers;
import java.io.File;
import net.elasticmc.sg.events.FrameworkEnable;
import net.elasticmc.sg.events.InventoryClick;
import net.elasticmc.sg.events.OneMinute;
import net.elasticmc.sg.events.OneSecond;
import net.elasticmc.sg.events.players.*;
import net.elasticmc.sg.game.GameManager;
import net.elasticmc.sg.game.LootManager;
import net.elasticmc.sg.lobby.LobbyManager;
import net.elasticmc.sg.maps.MapManager;
import net.elasticmc.sg.util.SimpleScoreboard;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import org.apache.commons.io.FileUtils;

/**
 * Created by Rushmead for ElasticMC
 */
public class ElasticSG extends JavaPlugin {

    private static ElasticSG instance;
    private MapManager mapManager;
    private GameManager gameManager;
    private LobbyManager lobbyManager;
    private LootManager lootManager;

    public static ElasticSG getInstance() {
        return instance;
    }

    public MapManager getMapManager() {
        return mapManager;
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    public LobbyManager getLobbyManager() {
        return lobbyManager;
    }

    public LootManager getLootManager() {
        return lootManager;
    }

    @Override
    public void onEnable() {
        instance = this;
        mapManager = new MapManager();
        gameManager = new GameManager();
        lobbyManager = new LobbyManager();
        lootManager = new LootManager();

        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new OneSecond(), this);
        pm.registerEvents(new OneMinute(), this);
        pm.registerEvents(new FrameworkEnable(), this);
        pm.registerEvents(new PlayerDeath(), this);
        pm.registerEvents(new PlayerInteract(), this);
        pm.registerEvents(new PlayerJoin(), this);
        pm.registerEvents(new PlayerLeave(), this);
        pm.registerEvents(new InventoryClick(), this);
        pm.registerEvents(new PlayerDamage(), this);
        pm.registerEvents(new BlockBreak(), this);
        pm.registerEvents(new BlockPlace(), this);
        mapManager.loadMaps();
        lootManager.loadLoot();
        mapManager.pickMap();

        Servers.getMap().setMapName(mapManager.getCurrentMap().getMapName());
        SimpleScoreboard.getManager().start();
    }

    @Override
    public void onDisable() {

        getServer().unloadWorld(mapManager.getCurrentMap().getWorldName(), false);
        try {
            File distFolder = new File(this.getServer().getWorldContainer().getAbsolutePath().replace(".", "") + "maps/" + mapManager.getCurrentMap().getWorldName());
            File mapFolder = new File(this.getServer().getWorldContainer().getAbsolutePath().replace(".", "") + mapManager.getCurrentMap().getWorldName());
            if (mapFolder.exists()) {
                mapFolder.delete();
                mapFolder.mkdirs();
            }

            FileUtils.copyDirectory(distFolder, mapFolder);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
