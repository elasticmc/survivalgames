package net.elasticmc.sg.commands;

import ch.playat.rushmead.elasticmc.database.players.PermissionSet;
import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import ch.playat.rushmead.elasticmc.framework.items.ItemUtil;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import net.elasticmc.sg.ElasticSG;
import net.elasticmc.sg.maps.Map;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Rushmead
 */
public class GameCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player && ElasticPlayer.getPlayer(((Player) sender).getName()).getPermissions().getPower() < PermissionSet.SNR_STAFF.getPower()) {
            Messaging.sendMessage(sender, "&cYou do not have the permissions to run this command.");
            return false;
        }
        if (args.length == 0) {
            Messaging.sendMessage(sender, "&cIncorrect usage: /game start/stop/restart");
            return false;
        }
        String subcommand = args[0];

        switch (subcommand) {
            case "start":
                ElasticSG.getInstance().getLobbyManager().startCountdown();
                break;
            case "force":
                ElasticSG.getInstance().getGameManager().prep();
                break;
            case "stop":
                ElasticSG.getInstance().getGameManager().stop();
                break;
            case "dm":
                ElasticSG.getInstance().getGameManager().startDeathmatch();
                break;
            case "reload":
                ElasticSG.getInstance().getLootManager().getLoot().clear();
                ElasticSG.getInstance().getLootManager().loadLoot();
                ElasticSG.getInstance().getLootManager().populateChests();
                break;
            case "item":
                sender.sendMessage(ItemUtil.toString(((Player) sender).getItemInHand()));
                break;
            case "world":
                Player player = (Player) sender;
                player.teleport(((Map) ElasticSG.getInstance().getMapManager().getMaps().values().toArray()[Integer.parseInt(args[1])]).getCenter());
        }

        return true;
    }

}
