package net.elasticmc.sg.commands;

import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.items.ItemUtil;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import net.elasticmc.sg.ElasticSG;
import net.elasticmc.sg.player.SGPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Rushmead
 */
public class SponsorCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (GameState.getGameState() == GameState.IN_GAME) {
            if (sender instanceof Player) {
                SGPlayer sGPlayer = SGPlayer.getPlayer(sender.getName());
                if (args.length > 0 && args.length <= 1) {
                    if (Bukkit.getPlayer(args[0]) != null) {
                        Player pl = Bukkit.getPlayer(args[0]);
                        if (ElasticSG.getInstance().getGameManager().spectators.contains(sender.getName())) {
                            if (sGPlayer.getSponsorTokens() > 0) {

                                Messaging.sendMessage(sender, "&aYou just sponsored &6&l{0}! &r&aThey will recieve a care package!", args[0]);
                                Messaging.sendMessage(pl, "&aYou were sponsored by &6&l{0}! &r&aA flare has been added to your inventory.", sender.getName());
                                ItemStack flare = ItemUtil.createItem(Material.REDSTONE_TORCH_ON, "&aCare Package", "&aPlace to call in CarePackage");
                                pl.getInventory().addItem(flare);
                                sGPlayer.removeSponsorToken();
                                return true;
                            } else {
                                Messaging.sendMessage(sender, "&4You do not have enough Sponsor Tokens.");
                                return false;
                            }
                        } else {
                            Messaging.sendMessage(sender, "&4Only Spectators can use this command.");
                            return false;
                        }

                    } else {
                        Messaging.sendMessage(sender, "&4That player does not exist.");
                        return false;
                    }

                } else {
                    Messaging.sendMessage(sender, "&4Incorrect usage: /sponsor <username>");
                    return false;
                }
            }

        } else {
            Messaging.sendMessage(sender, "&4This can only be used whilst in game.");
            return false;
        }
        return false;
    }
}
