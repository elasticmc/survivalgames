package net.elasticmc.sg.game;

import ch.playat.rushmead.elasticmc.database.Players;
import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import ch.playat.rushmead.elasticmc.framework.util.PlayerUtil;
import net.elasticmc.sg.ElasticSG;
import net.elasticmc.sg.player.SGPlayer;
import net.elasticmc.sg.util.RandomFirework;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.ChatPaginator;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

/**
 * @author Rushmead
 */
public class GameManager {

    public boolean countdown = false;
    public boolean deathmatch = false;
    public boolean refilled = false;
    public int countDownTime = 10;
    public int gameTime = 600;
    public int deathMatchTimer = 300;
    public int refillTime = 150;
    public ArrayList<String> players = new ArrayList<>();
    public ArrayList<String> spectators = new ArrayList();
    public ConcurrentHashMap<Location, Integer> carePackages = new ConcurrentHashMap<>();

    public void start() {
        for (ElasticPlayer ep : ElasticPlayer.getPlayers().values()) {
            ep.setFrozen(false);
            PlayerUtil.sendTitleOrSubtitle(ep.getPlayer(), PacketPlayOutTitle.EnumTitleAction.TITLE, "GAME STARTED", 0, 20, 0, ChatColor.DARK_PURPLE);
        }
        Messaging.broadcastMessage("&5GAME STARTED!");

        GameState.setGameState(GameState.IN_GAME);
        countdown = false;
    }

    public void dmStrt() {
        for (ElasticPlayer ep : ElasticPlayer.getPlayers().values()) {
            ep.setFrozen(false);
            PlayerUtil.sendTitleOrSubtitle(ep.getPlayer(), PacketPlayOutTitle.EnumTitleAction.TITLE, "DEATHMATCH STARTED", 0, 20, 0, ChatColor.DARK_PURPLE);
        }
        Messaging.broadcastMessage("&aDEATHMATCH STARTED!");

        countdown = false;
    }

    public void stop() {
        GameState.setGameState(GameState.ENDING);
        final Player winner = Bukkit.getPlayer(players.get(0));
        String bullet = "\u2022";
        String bullets = ChatColor.DARK_GRAY + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet;
        Messaging.broadcastMessage("&5GAME OVER!");
        Messaging.broadcastRawMessage(title(bullets));

        Messaging.broadcastRawMessage(title("&4Congrats to {0} on winning!"), winner.getName());
        for (Player p : Bukkit.getOnlinePlayers()) {
            Messaging.sendRawMessage(p, title("{0}{1} Kills: {2}"), ChatColor.GOLD, "Your", SGPlayer.getPlayer(p.getName()).getCurrentKills());
            Messaging.sendRawMessage(p, title("{0}ElasticBands Earnt: {2}"), ChatColor.GOLD, "Your", SGPlayer.getPlayer(p.getName()).getCurrentKills() * 2);
            int current = Players.getBands().getElasticBands(p.getName());
            Players.getBands().setBands(p.getName(), current + SGPlayer.getPlayer(p.getName()).getCurrentKills() * 2);

        }
        Bukkit.getScheduler().scheduleSyncRepeatingTask(ElasticSG.getInstance(), new Runnable() {

            @Override
            public void run() {
                RandomFirework.spawnRandomFirework(winner.getLocation());
            }
        }, 20L, 20L);
        Messaging.broadcastRawMessage(title("&5Restarting Server in 10 Seconds...."));
        Messaging.broadcastRawMessage(title(bullets));
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "restartserver 10");
    }

    public String title(String text) {

        String title = "";

        for (int x = 0; x <= ((ChatPaginator.AVERAGE_CHAT_PAGE_WIDTH / 2) - text.length()); x++) {
            title += " ";
        }

        title += text;

        for (int x = 0; x <= ((ChatPaginator.AVERAGE_CHAT_PAGE_WIDTH / 2) - text.length()); x++) {
            title += " ";
        }

        return title;

    }

    public void startCountdown() {
        GameState.setGameState(GameState.SETUP);
        countdown = true;
        countDownTime = 10;
    }

    public void startDMCountdown() {
        countdown = true;
        countDownTime = 10;

    }

    public void togglePlayerVisibility(Player p, boolean visible) {
        for (Player o : Bukkit.getOnlinePlayers()) {
            if (visible) {
                p.showPlayer(o);
            } else {
                p.hidePlayer(o);
            }
        }
    }

    public static void spawnRandomFirework(final Location loc) {
        final Firework firework = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
        final FireworkMeta fireworkMeta = firework.getFireworkMeta();
        final Random random = new Random();
        final FireworkEffect effect = FireworkEffect.builder().flicker(random.nextBoolean()).withColor(getColor(random.nextInt(17) + 1)).withFade(getColor(random.nextInt(17) + 1)).with(Type.values()[random.nextInt(Type.values().length)]).trail(random.nextBoolean()).build();
        fireworkMeta.addEffect(effect);
        fireworkMeta.setPower(random.nextInt(2) + 1);
        firework.setFireworkMeta(fireworkMeta);
    }

    private static Color getColor(final int i) {
        switch (i) {
            case 1:
                return Color.AQUA;
            case 2:
                return Color.BLACK;
            case 3:
                return Color.BLUE;
            case 4:
                return Color.FUCHSIA;
            case 5:
                return Color.GRAY;
            case 6:
                return Color.GREEN;
            case 7:
                return Color.LIME;
            case 8:
                return Color.MAROON;
            case 9:
                return Color.NAVY;
            case 10:
                return Color.OLIVE;
            case 11:
                return Color.ORANGE;
            case 12:
                return Color.PURPLE;
            case 13:
                return Color.RED;
            case 14:
                return Color.SILVER;
            case 15:
                return Color.TEAL;
            case 16:
                return Color.WHITE;
            case 17:
                return Color.YELLOW;
        }
        return null;
    }

    public void prep() {
        Bukkit.getWorld(ElasticSG.getInstance().getMapManager().getCurrentMap().getWorldName()).setTime(0);
        for (Player player : Bukkit.getOnlinePlayers()) {
            SGPlayer sgPlayer = SGPlayer.getPlayer(player.getDisplayName());
            ElasticPlayer elasticPlayer = ElasticPlayer.getPlayer(player.getDisplayName());
            player.getInventory().clear();
            player.setGameMode(GameMode.SURVIVAL);
            player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, Integer.MAX_VALUE, 1));
            elasticPlayer.setFrozen(true);
            togglePlayerVisibility(player, true);
        }
        for (int i = 0; i < players.size(); i++) {
            Location podium = ElasticSG.getInstance().getMapManager().getCurrentMap().getSpawnPoints().get(i);
            Player pl = Bukkit.getPlayer(players.get(i));
            pl.teleport(podium);
        }
        for (Player player : Bukkit.getOnlinePlayers()) {
            SGPlayer sgPlayer = SGPlayer.getPlayer(player.getDisplayName());
            ElasticPlayer elasticPlayer = ElasticPlayer.getPlayer(player.getDisplayName());
            player.removePotionEffect(PotionEffectType.BLINDNESS);
        }
        startCountdown();
    }

    public void startDeathmatch() {
        deathmatch = true;
        for (String str : players) {
            Player player = Bukkit.getPlayer(str);
            SGPlayer sgPlayer = SGPlayer.getPlayer(player.getDisplayName());
            ElasticPlayer elasticPlayer = ElasticPlayer.getPlayer(player.getDisplayName());
            player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, Integer.MAX_VALUE, 1));
            elasticPlayer.setFrozen(true);
        }
        for (int i = 0; i < players.size(); i++) {
            Location podium = ElasticSG.getInstance().getMapManager().getCurrentMap().getSpawnPoints().get(i);
            Player pl = Bukkit.getPlayer(players.get(i));
            pl.teleport(podium);
        }
        for (String player : players) {
            SGPlayer sgPlayer = SGPlayer.getPlayer(player);
            ElasticPlayer elasticPlayer = ElasticPlayer.getPlayer(player);
            Player pl = Bukkit.getPlayer(player);
            pl.removePotionEffect(PotionEffectType.BLINDNESS);
        }
        startDMCountdown();
    }
}
