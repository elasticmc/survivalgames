package net.elasticmc.sg.game;

import ch.playat.rushmead.elasticmc.database.DatabaseConnections;
import ch.playat.rushmead.elasticmc.framework.items.ItemUtil;
import net.elasticmc.sg.ElasticSG;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import org.bukkit.Location;

/**
 * Created by Rushmead for ElasticMC
 */
public class LootManager {

    private HashMap<ItemStack, Integer> loot = new HashMap<ItemStack, Integer>();
    private ArrayList<Location> chests = new ArrayList<>();

    public ArrayList<Location> getChests() {
        return chests;
    }

    public HashMap<ItemStack, Integer> getLoot() {
        return loot;
    }

    public void loadLoot() {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `loot` WHERE `game`=?");
            ps.setString(1, "sg");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                loot.put(ItemUtil.fromString(rs.getString("itemstack")), rs.getInt("chance"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void populateChests() {
        World w = Bukkit.getWorld(ElasticSG.getInstance().getMapManager().getCurrentMap().getWorldName());
        for (Chunk c : w.getLoadedChunks()) {
            for (BlockState b : c.getTileEntities()) {
                if (b instanceof Chest) {
                    ((Chest) b).getBlockInventory().clear();
                    for (int i = 0; i < 3; i++) {
                        fillChests(((Chest) b).getBlockInventory());
                        chests.add(b.getLocation());
                    }
                }
            }
        }
    }

    public void fillChests(Inventory inv) {
        inv.clear();
        for (int i = 1; i < 4; i++) {
            Random slotnum = new Random();
            int slot = slotnum.nextInt(inv.getSize());
            inv.setItem(slot, getRandomItemStack());
        }
    }

    public final ItemStack getRandomItemStack() {
        Random rand = new Random();
        int totalSum = 0;
        ArrayList<ItemStack> items = new ArrayList(loot.keySet());
        for (ItemStack item : items) {
            totalSum = totalSum + loot.get(item);
        }
        int index = rand.nextInt(totalSum);
        int sum = 0;
        int i = 0;
        while (sum < index) {
            sum += loot.get(items.get(i++));
            if (items.size() <= i) {
                index = rand.nextInt(totalSum);
                i = 0;
            }
        }
        return items.get(i);
    }

    public int getRandom(int a, int b) {
        return (int) Math.round((Math.random() * b) + a);
    }
}
