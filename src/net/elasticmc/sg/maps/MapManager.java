package net.elasticmc.sg.maps;

import ch.playat.rushmead.elasticmc.database.DatabaseConnections;
import net.elasticmc.sg.ElasticSG;
import org.bukkit.*;
import org.bukkit.block.Block;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Rushmead
 */
public class MapManager {

    private HashMap<String, Map> maps = new HashMap<String, Map>();
    private Map currentMap;

    public synchronized void loadMaps() {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `maps` WHERE `game`=?");
            ps.setString(1, "sg");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Map m = new Map(rs.getString("name"), rs.getString("authors"), getDeserializedLocation(rs.getString("spawnLocation1"), rs.getString("worldname")), getPoduims(getDeserializedLocation(rs.getString("spawnLocation1"), rs.getString("worldname"))), rs.getString("worldname"));
                maps.put(rs.getString("name"), m);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Location> getPoduims(Location center) {
        List<Location> list = new ArrayList<>();

        Location origin = center; // some origin point
        int r = 20; // radius
        for (Block b : getCircleBlocks(center.getBlock(), 200, Axis.Y)) {
            if (b.getType() == Material.BEDROCK) {
                System.out.print("Found Bedrock...");
                list.add(b.getLocation());
            }
        }
        return list;
    }

    public enum Axis {

        X, Y, Z
    }

    public static List<Block> getCircleBlocks(Block center, int radius, Axis axis) {
        List<Block> result = new ArrayList<Block>();

        // find our diameter, always odd since we need a center block
        int diameter = (2 * radius) + 1;

        int bx = center.getX();
        int by = center.getY();
        int bz = center.getZ();

        // we'll need this later
        World w = center.getWorld();

        // this should always be true, but check anyway
        if (w != null) {
            // time to find the corner block to search through
            int cx = bx - radius;
            int cy = by - radius;
            int cz = bz - radius;

            // now we cycle!
            for (int i = 0; i < diameter; i++) {
                for (int j = 0; j < diameter; j++) {
                    int tx;
                    int ty;
                    int tz;

                    switch (axis) {
                        case X:
                            ty = cy + i;
                            tz = cz + j;

                            if (Math.round(Math.sqrt(
                                    Math.pow(by - ty, 2) + Math.pow(bz - tz, 2)
                            )) <= radius) {
                                // gotta get this block and return it!
                                Block b = w.getBlockAt(center.getX(), ty, tz);
                                result.add(b);
                            }
                            break;
                        case Z:
                            tx = cx + i;
                            ty = cy + j;

                            if (Math.round(Math.sqrt(
                                    Math.pow(bx - tx, 2) + Math.pow(by - ty, 2)
                            )) <= radius) {
                                // gotta get this block and return it!
                                Block b = w.getBlockAt(tx, ty, center.getZ());
                                result.add(b);
                            }
                            break;
                        default:
                        case Y:
                            tx = cx + i;
                            tz = cz + j;

                            if (Math.round(Math.sqrt(
                                    Math.pow(bx - tx, 2) + Math.pow(bz - tz, 2)
                            )) <= radius) {
                                // gotta get this block and return it!
                                Block b = w.getBlockAt(tx, center.getY(), tz);
                                result.add(b);
                            }
                            break;
                    }
                }
            }
        }

        return result;
    }

    public void pickMap() {
        Random generator = new Random();
        Object[] values = maps.values().toArray();
        Map randomValue = (Map) values[generator.nextInt(values.length)];
        currentMap = randomValue;
        ElasticSG.getInstance().getServer().getWorld(randomValue.getWorldName()).setSpawnLocation(randomValue.getCenter().getBlockX(), randomValue.getCenter().getBlockY(), randomValue.getCenter().getBlockZ());
        ElasticSG.getInstance().getLootManager().populateChests();
    }

    public Map getCurrentMap() {
        return currentMap;
    }

    public Map getMapByName(String name) {
        return maps.get(name);
    }

    public HashMap<String, Map> getMaps() {
        return maps;
    }

    public String getSerializedLocation(Location loc) { //Converts location -> String
        return loc.getX() + ";" + loc.getY() + ";" + loc.getZ();
        //feel free to use something to split them other than semicolons (Don't use periods or numbers)
    }

    public Location getDeserializedLocation(String s, String world) {//Converts String -> Location

        String[] parts = s.split(";"); //If you changed the semicolon you must change it here too
        double x = Double.parseDouble(parts[0]);
        double y = Double.parseDouble(parts[1]);
        double z = Double.parseDouble(parts[2]);
        System.out.print(x);
        System.out.print(y);
        System.out.print(z);
        WorldCreator wc = new WorldCreator(world);
        ElasticSG.getInstance().getServer().createWorld(wc);
        World w = Bukkit.getServer().getWorld(world);
        w.setAutoSave(false);
        w.setDifficulty(Difficulty.PEACEFUL);
        w.setGameRuleValue("doMobSpawning", "false");
        w.setDifficulty(Difficulty.EASY);
        return new Location(w, x, y, z); //can return null if the world no longer exists
    }
}
