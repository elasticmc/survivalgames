package net.elasticmc.sg.maps;

import org.bukkit.Location;

import java.util.List;

/**
 *
 * @author Rushmead
 */
public class Map {

    private String mapName;
    private String authors;
    private Location center;
    private List<Location> spawnPoints;
    private String worldName;
    public Map(String mapName, String authors, Location center, List<Location> spawnPoints, String worldName) {
        this.mapName = mapName;
        this.authors = authors;
        this.center = center;
        this.spawnPoints = spawnPoints;
        this.worldName = worldName;
    }

    /**
     * @return the mapName
     */
    public String getMapName() {
        return mapName;
    }

    /**
     * @param mapName the mapName to set
     */
    public void setMapName(String mapName) {
        this.mapName = mapName;
    }

    /**
     * @return the authors
     */
    public String getAuthors() {
        return authors;
    }

    /**
     * @param authors the authors to set
     */
    public void setAuthors(String authors) {
        this.authors = authors;
    }


    public Location getCenter() {
        return center;
    }

    public void setCenter(Location center) {
        this.center = center;
    }

    public List<Location> getSpawnPoints() {
        return spawnPoints;
    }

    public void setSpawnPoints(List<Location> spawnPoints) {
        this.spawnPoints = spawnPoints;
    }

    public String getWorldName() {
        return worldName;
    }
}
